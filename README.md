切图网发布 SLICY移动切图框架 v1.0版本。 这是继 SLICY PC响应式框架后 一款专门针对  WEBAPP移动网站的 一款简约、优雅的HTML5/CSS3框架。

优势
SLICY移动切图框架 ，可以让你像布局PC网站一样的操作移动项目，相比目前很多移动端网站， 在手机版会拉升的很长，甚至变形，或者文字界面被放大，在这一点上SLICY 处理的很优雅。

特点
采用WEBAPP 主流宽度 640宽度
采用REM单位布局 ，完美自适应主流浏览器屏幕
提供主流的表单元件的美化解决方案

使用
css
<link rel="stylesheet" href="assets/css/slicy.css"  />

js
<script src="assets/js/slicy.js" ></script>

案例
目前该框架部分或者全部已经用到了 腾讯、智联、华为等项目中。

文档
http://m.slicy.cn

下载
http://www.qietu.cn/thread-15240-1-1.html
